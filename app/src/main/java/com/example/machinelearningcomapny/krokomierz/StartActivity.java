package com.example.machinelearningcomapny.krokomierz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity {

    public Button startButton;
    private EditText weight;
    public String textWeight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        startButton = (Button) findViewById(R.id.buttonEnter);
        weight = (EditText) findViewById(R.id.weightEditText);
    }

    public void enter(View v){
        textWeight = weight.getText().toString();
        if(textWeight.matches("")){
            Toast.makeText(this,"Prosze wprowadzic wage",Toast.LENGTH_SHORT).show();
        }
        else{

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("weight",textWeight);
            startActivity(intent);
        }
    }
}
