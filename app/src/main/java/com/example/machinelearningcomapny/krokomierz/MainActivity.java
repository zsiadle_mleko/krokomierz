package com.example.machinelearningcomapny.krokomierz;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {

    //Buttons start & pause
    private Button startButton;
    private Button pauseButton;

    // Display Field for Sensitivity
    private TextView textSensitive;

    // Display for Steps
    private TextView textViewSteps;

    // Reset Button
    private Button buttonReset;

    // Sensor Manager
    private SensorManager sensorManager;
    private float acceleration;

    // Values to Calculate Number of Steps
    private float previousY;
    private float currentY;
    private int numSteps;

    // Seekbar Fields
    private SeekBar seekBar;
    private int threshold; // Point at which we want to trigger a 'step'
    private TextView timerText;


    long starttime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedtime = 0L;
    int t = 1;
    int secs = 0;
    int mins = 0;
    int milliseconds = 0;
    Handler handler = new Handler();


    public double weight;
    public double kcal;
    public TextView kcalTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        String textWeight = extras.getString("weight");
        weight = Float.valueOf(textWeight);
       // Log.i("TUTAJ WAGA",String.valueOf(weight));
        kcalTextView = (TextView) findViewById(R.id.kcal);
        //kcalTextView.setText("Spalone kalorie: "+String.valueOf(weight));

        // Attach objects to XML View
        //textViewX = (TextView) findViewById(R.id.textViewX);
        //textViewY = (TextView) findViewById(R.id.textViewY);
        //textViewZ = (TextView) findViewById(R.id.textViewZ);

        // Attach Step and Sensitivity View Objects to XML
        textViewSteps = (TextView) findViewById(R.id.textSteps);
        textSensitive = (TextView) findViewById(R.id.textSensitive);

        // Attach the resetButton to XML
        buttonReset = (Button) findViewById(R.id.buttonReset);

        // Attach the seekBar to XML
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        // Set the Values on the seekBar, threshold and threshold display
        seekBar.setProgress(10);
        seekBar.setOnSeekBarChangeListener(seekBarListener);
        threshold = 10;
        textSensitive.setText(String.valueOf(threshold));

        // Initialize Values
        previousY = 0;
        currentY = 0;
        numSteps = 0;

        // Initialize acceleration Values
        acceleration = 0.00f;

        startButton = (Button) findViewById(R.id.buttonStart);
       // pauseButton = (Button) findViewById(R.id.buttonPause);
        //pauseButton.setBackgroundColor(Color.RED);
        //startButton.setBackgroundColor(Color.LTGRAY);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        timerText = (TextView) findViewById(R.id.stoper);
        //Enable the listener
        //enableAccelerometerListening();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void enableAccelerometerListening(){
        // Initialize the Sensor Manager
       // sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //Sensor mAccelerometer;
       // mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
       // sensorManager.registerListener(sensorEventListener,mAccelerometer,sensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), sensorManager.SENSOR_DELAY_NORMAL);
    }

    private void disableAccelerometerListening(){
        sensorManager.unregisterListener(sensorEventListener);
    }

    //Event handler for accelerometer events
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            //Gather the values form accelerometer
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            //Fetch the current y
            currentY = y;

            // Measure if a step is taken
            if ( Math.abs(currentY - previousY) > threshold ){
                numSteps++;
                textViewSteps.setText(String.valueOf(numSteps));
            }

            // Display the values
            //textViewX.setText(String.valueOf(x));
            //textViewY.setText(String.valueOf(y));
            //textViewZ.setText(String.valueOf(z));

            // Store the previous Y
            previousY = y;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public void startMeasure(View v){



        if (t == 1) {
            enableAccelerometerListening();
            startButton.setText("Pause");
            starttime = SystemClock.uptimeMillis();
            handler.postDelayed(updateTimer, 0);
            t = 0;
        } else {
            disableAccelerometerListening();
            startButton.setText("Start");
            timerText.setTextColor(Color.RED);
            timeSwapBuff += timeInMilliseconds;
            handler.removeCallbacks(updateTimer);
            t = 1;
        }


}



    public void resetSteps(View v){
        numSteps = 0;
        textViewSteps.setText(String.valueOf(numSteps));

        starttime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedtime = 0L;
        t = 1;
        secs = 0;
        mins = 0;
        milliseconds = 0;
        startButton.setText("Start");
        timerText.setTextColor(Color.RED);
        timerText.setText("0:00.000");
        kcalTextView.setText("Spalone kalorie: 0");
        handler.removeCallbacks(updateTimer);




    }

    private OnSeekBarChangeListener seekBarListener =
            new OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    threshold = seekBar.getProgress();
                    textSensitive.setText(String.valueOf(threshold));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };


    public Runnable updateTimer = new Runnable() {
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - starttime;
            updatedtime = timeSwapBuff + timeInMilliseconds;
            secs = (int) (updatedtime / 1000);
            mins = secs / 60;
            secs = secs % 60;
            milliseconds = (int) (updatedtime % 1000);
            timerText.setText("" + mins + ":" + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));
            timerText.setTextColor(Color.BLUE);
            kcal = 0.3 * weight * 2.20462262185 * ((double)secs/60);
            kcalTextView.setText("Spalone kalorie: "+String.valueOf(new DecimalFormat("###.##").format(kcal)));
            handler.postDelayed(this, 0);
        }};

}
